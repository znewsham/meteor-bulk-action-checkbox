# znewsham:bulk-action-checkboxes

Add a checkbox with an associated dropdown menu to facilitate bulk actions

## Usage

`meteor add znewsham:bulk-action-checkboxes`

See [this example](https://bitbucket.org/znewsham/table-bulk-demo) of the usage of this component and a dynamic table component

```html
{{> bulkActionCheckbox bulkActionCheckboxOptions}}
```

```js
bulkActionCheckboxOptions(selectAll = false){
  const templInstance = Template.instance();
  return {
    set: "my-set",//required - denotes the set of checkboxes this belongs to, to allow for a select-all to toggle.
    target: "my-document-id",
    value: "some-value",
    title: "",//optional - will be displayed between the checkbox and the dropdown
    selectAll: false,//optional - will cause it to toggle all checkboxes of the same set.
    checkboxClass: "my-checkbox", //optional - for styling and/or external events

    //optional - applies styling to the dropdown menu - primarily for specifying a width. Could also be done with raw CSS
    style: {
      dropdown: "width: 250px;"
    },

    //required - the list of options and associated functions
    options: [
      {
        label: "My Cool Function", //required - the label of the option
        //optional - a callback event,
        //the arguments will either be a single target/value in the case that the dropdown is used directly,
        //or arrays in the case that the "select-all" checkbox is used.
        callback(target, value){

        },
        class: "my-cool-function-class" //optional - to allow for external event handling
      }
    ]
  };
}
```
