Package.describe({
  name: 'znewsham:bulk-action-checkbox',
  version: '0.1.7',
  // Brief, one-line summary of the package.
  summary: 'Add a checkbox with an associated dropdown menu to facilitate bulk actions',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/znewsham/meteor-bulk-action-checkbox',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom(["METEOR@1.4"]);
  api.use([
    "ecmascript",
    "underscore",
    "blaze",
    "templating",
  ]);

  // jquery is a weak reference in case you want to use a different package or
  // pull it in another way, but regardless you need to make sure it is loaded
  // before any tabular tables are rendered
  api.use(["jquery"], "client", { weak: true });
  api.mainModule('bulkActionCheckbox.js', 'client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('znewsham:bulk-action-checkbox');
  api.mainModule('bulk-action-checkbox-tests.js');
});
