import './bulkActionCheckbox.html';
import './bulkActionCheckbox.css';

Meteor.startup(() => {
  $(document).mousedown((e) => {
    if (!$(e.target).parents(".bulkActionCheckbox").length) {
      $(".bulkActionCheckbox").removeClass("active");
    }
  });
});
Template.bulkActionCheckbox.onRendered(function onRendered() {

  this.autorun((comp) => {
    const data = Template.currentData();
    if (comp.firstRun) {
      return;
    }
    this.$("input").prop("checked", false);
  });
});
Template.bulkActionCheckbox.helpers({
  options(){
    return Template.instance().data.options;
  }
});

Template.bulkActionCheckbox.events({
  "change input"(e){
    const data = Template.instance().data;
    if(data.selectAll){
      e.stopPropagation();
      $("input[data-set=" + data.set + "]").prop("checked", $(e.currentTarget).is(":checked"));
    }
  },
  "click .bulkActionCheckbox"(e) {
    e.stopPropagation();
    if(e.target.tagName.toLowerCase() === "input"){
      return;
    }
    e.preventDefault();
		const hadClass = Template.instance().$(".bulkActionCheckbox").hasClass("active");
		$(".bulkActionCheckbox").removeClass("active");
		if(!hadClass){
      Template.instance().$(".bulkActionCheckbox").toggleClass("active");
		}
  },
  "click li.dropdown-option"(e) {
    const index = $(e.currentTarget).attr("data-index");
    const data = Template.instance().data;
		const target = data.selectAll ? _.toArray($(`input[data-set=${data.set}]:checked`)).filter(cb => cb !== Template.instance().$("input")[0]).map(cb => $(cb).attr("data-target")) : data.target;
		const value = data.selectAll ? _.toArray($(`input[data-set=${data.set}]:checked`)).filter(cb => cb !== Template.instance().$("input")[0]).map(cb => $(cb).val()) : data.value;
		if(data.options[index].callback){
      e.preventDefault();
      e.stopPropagation();
      $("input[data-set=" + data.set + "]").prop("checked", false);
      data.options[index].callback.call(e.currentTarget, target, value);
		}
    Template.instance().$(".bulkActionCheckbox").removeClass("active");
  }
});
