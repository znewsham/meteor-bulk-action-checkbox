// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by bulk-action-checkbox.js.
import { name as packageName } from "meteor/znewsham:bulk-action-checkbox";

// Write your tests here!
// Here is an example.
Tinytest.add('bulk-action-checkbox - example', function (test) {
  test.equal(packageName, "bulk-action-checkbox");
});
